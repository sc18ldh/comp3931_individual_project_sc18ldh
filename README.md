# COMP3931_Individual_Project_SC18LDH

Project files for final year project of 'Extending the Capabilities of Sampling-Based Planners for Robots in Human-Populated Environments'

Source code for motion planner approach 1 and 2 as well as the RRT-Connect testing motion planner are available in the 'src' folder. Also included is simAddOnScript-runProjectTrials.lua which is the script developed to automatically run trials within CoppeliaSim 

The 'scenes' folder contains all of the CoppeliaSim simulation scenes used for evalutation and functional testing for each of the developed motion planners

user_manual.pdf contains instructions on how to run the motion planning approaches within CoppeliaSim
