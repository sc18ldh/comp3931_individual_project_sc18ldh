-- this add-on simply adds an alternative UI that allows to start/stop/pause a simulation. This is not very useful,
-- but illustrates how easily CoppeliaSim can be customized using add-ons
-- return sim.syscb_cleanup if you want to stop the add-on script from here!

function sysCall_init()
    sim.addStatusbarMessage("Initialization of the add-on script")
    max_num_trials=50 --number of trials to run
    num_trials_run=0 --number of trials run so far
    sim.addStatusbarMessage("ADDON: number of trials to run is: "..max_num_trials )

    noEditMode=sim.getInt32Parameter(sim.intparam_edit_mode_type)==0
end

function sysCall_cleanup()
    sim.addStatusbarMessage("Clean-up of the add-on script")
end

function sysCall_addOnScriptSuspend()
    sim.addStatusbarMessage("Suspending add-on script")
end

function sysCall_addOnScriptResume()
    sim.addStatusbarMessage("Restarting the add-on script")
end

function sysCall_nonSimulation()

end

function sysCall_beforeMainScript()

end

function sysCall_beforeInstanceSwitch()
    sim.addStatusbarMessage("Before switching to another instance (add-on script)")
end

function sysCall_afterInstanceSwitch()
    sim.addStatusbarMessage("After switching to another instance (add-on script)")
end

function sysCall_suspend()
    sim.addStatusbarMessage("Suspending simulation (add-on script)")
end

function sysCall_resume()
    sim.addStatusbarMessage("Resuming simulation (add-on script)")
end

function sysCall_beforeSimulation()
    sim.addStatusbarMessage("Simulation starting (add-on script)")
    --if the max number of trials have been run then stop the simulation
    if num_trials_run>=max_num_trials then
        sim.addStatusbarMessage("ADDON: max number of trials reached")
        sim.stopSimulation()
    end
end

function sysCall_afterSimulation()
    num_trials_run=num_trials_run+1
    --if the max number of trials haven't been reached then run trial again
    if num_trials_run<max_num_trials then
        sim.addStatusbarMessage("Simulation ended (add-on script)")
        sim.addStatusbarMessage("ADDON: number of trials run: "..num_trials_run)
        sim.startSimulation()
    end
end


function addOnClose()
    exitRequest=true
end

function startSim()
    sim.startSimulation()
end

function suspendSim()
    sim.pauseSimulation()
end

function stopSim()
    sim.stopSimulation()
end
