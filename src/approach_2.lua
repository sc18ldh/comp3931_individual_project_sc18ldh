function sysCall_init()
    --********************************OBJECT HANDLE ASSIGNMENT VARIABLES**************************************
    --robot handles
    robot=sim.getObjectHandle('Pioneer_p3dx')
    robot_collection_handle=sim.getCollectionHandle("robot_collection")
    lm=sim.getObjectHandle('Pioneer_p3dx_leftMotor')
    rm=sim.getObjectHandle('Pioneer_p3dx_rightMotor')

    --motion control handles
    start_dummy_handle=sim.getObjectHandle('LookAheadDummy')
    robot_collision_detection_point_dummy_handle=sim.getObjectHandle('RobotCollisionDetectionDummy')
    path_collision_detection_point_dummy_handle=sim.getObjectHandle('PathCollisionDetectionDummy')
    closest_point_on_path_dummy=sim.getObjectHandle('ClosestPointDummy') --dummy indicating closest point on path

    --path planning handles
    start_config_cylinder_handle=sim.getObjectHandle('Start_Config')
    end_goal_config_dummy_handle=sim.getObjectHandle('EndGoalConfiguration')
    second_goal_config_dummy_handle=sim.getObjectHandle('SecondGoalConfiguration')

    --motion control variables===================================================================================================
    vel_des=0.4 --desired robot linear velocity
    gain=0.08 --constant of proportionality for veloicity and steering error
    minimum_movement_vel=0.05 --minimum linear velocity the robot should ever have (exept when stopped)

    rob_look_ahead_dis=0.3 --minimum distance next point should be away for Pure Pursuit algorithm

    wheel_sep=0.19 --wheel seperation (axel)
    wheel_rad=0.195/2 -- robot's wheel radius

    --********************************PATH PLANNING VARIABLES********************************************
    init_rob_pos=sim.getObjectPosition(robot,-1) --start position of the robot
    --initial position of cylinder used to represent Pioneer 3-DX for path planning
    init_robot_cylinder_pos=sim.getObjectPosition(start_config_cylinder_handle,-1)
    init_robot_cylinder_orient=sim.getObjectOrientation(start_config_cylinder_handle,-1)

    rob_point_step_dis=0.07 --distance of robot from point that point in path is incremented when searching

    init_robot_jump=18 --number of states to jump upon creating path (needs to be multiple of 3 e.g. 6 skips 2 states)
    rob_look_ahead_jump=6 --number of states to jump (needs to be multiple of 3 e.g. 6 skips 2 states)

    is_moving_thresh=0.005 --linear velocity theshold where an object is considered to be moving

    max_robot_deviation_from_path=0.5 --maximum in m that robot can be off path before replanning

    generate_new_primary_path=true --flag to state whether a new primary path needs to be created by OMPL
    generate_new_secondary_path=false --flag to state whether a new secondary path needs to be created by OMPL

    follow_secondary_path=false --when true when robot should follow secondary path

    end_goal_reached=false --has the end goal been reached by the robo
    goal_dis_thresh=0.2 --distance robot has to get within before goal is classed as reached

    secondary_goal_point=nil --goal point for secondary path (initialised to nil)
    secondary_goal_index_on_primary_path=nil --index that secondary goal placed is on primary path

    --counter and index variables
    i=1 --index for position on primary OMPL path
    s=1 --index for position on secondary OMPL path
    num_paths_generated_since_collision=0 --number of times a path has been generated since a detected collision

    --********************************TIME KEEPING VARIABLES********************************************
    last_time_rate=sim.getSimulationTime() --used to find delta time between last execution cycle
    rate=0.25 --rate at which all the main code should be run at in (s)

    wait_for_delay=false --if execution should of the main program should wait to compensate for OMPL calculation
    last_failed_path_time=nil --last sim time a path was failed to be planned

    simulation_start_time=sim.getSimulationTime() --time simulation started in sim time
    system_start_time=sim.getSystemTime() --get time simulation started in system time (s)

    --********************************COLLISION CHECKING VARIABLES***************************************
    num_bills_in_scene=10 --number of bill's humans in the current scene ADJUST THIS TO WORKSPACE!

    --assign all bills' an object handle and store in table
    bill_handles={}
    bill_handles=createBillObjectHandles(num_bills_in_scene) --assigns handles for all bills in scene

    bill_radius=0.35 --safety radius from bill's centre point
    last_collision_bill_h=-1 --handle of bill most recently detected in collision on path

    --collision detection point variables
    collision_radius=0.2 --collision radius to check for in m for collision dummy
    safety_distance=0.7 --distance to place collision dummy out from robot

    index_collision_detected_at=nil --index colliion was detected at on path

    --collision time variables
    last_time_col=0 --last time a collision was detected for a NEW Bill
    col_forget_time=7 --time to forget to reset about last collision in seconds

    --collision checking sphere variables
    check_sphere_dummys_h={} --table to hold handles for all colission checking spheres along path
    collision_detection_points_placed=0 --number of collision detection_points_placed in scene
    max_num_of_col_detection_points=3 --max num of points to search along path before declaring collision free

    --setting initial robot motor velocities to 0 to prevent initial movement
    sim.setJointTargetVelocity(rm,0)
    sim.setJointTargetVelocity(lm,0)

    --**********TEST AND EVALUATION VARIABLES******************
    test_dummy=sim.getObjectHandle('TestDummy')

    sensing_rate=0.1 --time rate to check testing variables

    sim_is_stopping=false ---if the simulator is stopping due to going over max time

    max_time_allowed_to_reach_goal=360 --maxiumum time the robot is allowed to reach the goal before the trial ends

    --collision with humans
    time_to_wait_between_collisions=1.0 --number of seconds to wait before recording a new collision
    last_collision_time=sim.getSimulationTime()
    number_of_collisions_with_humans=0

    --distance covered
    robot_last_position=sim.getObjectPosition(robot,-1)
    total_distance_traveled=0

    --calculating total number of paths computed
    total_num_paths_computed=0

    --calculating total time spent planning paths
    total_planning_time=0

    --file saving variables
    file_name="Approach2_warehouse_10bills_50x.txt"
end

--[[
When given number of bills (human models) in the scene returns a table containing
specified number of bill object handles

Arguments:
    num_bills::int
        number of bills in the robot's workspace to add to table

Returns:
    b_handles::table
        table containing located bill object handles
]]--
function createBillObjectHandles(num_bills)
    local b=1
    local b_handles = {}
    for b=1,num_bills,1 do
    b_handles[b]=sim.getObjectHandle("Bill#"..(b-1)) --adding bill handle to table
    end
    return b_handles
end

--[[
When given a collision detection point checks if any of the Bill's provided as
arguments are within the specified collision distance and returns true if
they are and false otherwise

Arguments:
    b_handles::table
        table containing bill object handles to check if in collision radius
    col_dis::float
        collision radius to check for in m
    col_dummy:int(object handle)
        object handle of specified collision detection point dummy

Returns:
      true
          if any bills in b_handles are within col_dis of the speciied collision
          detection point
      false
          if all bills in b_handles are outside of collision radius
]]--
function isBillInCollisionRadius(b_handles,col_dis,col_dummy)
    local index=1 --index for bill handle
    local is_collision=false --set to true if there has been a collision
    local res,dis_tab --result and distance table
    for i=1,#b_handles,1 do
        index=i
        --checking distance between col dummy and bills
        res,dis_tab=sim.checkDistance(col_dummy,b_handles[i],(col_dis+bill_radius))
        --if minimum distance is less than threshold then set collision flag
        if res==1 then
           is_collision=true
           break
        end
    end

    --if there is a collision return true flag and index of colliding bill in table
    if is_collision==true then
        return 1, index
    --if there is no colision then return false flag and omit index
    else
        return 0,nil
    end
end

--[[
Determines if any of the Bill's are inside the collision radius should be planned
around or not. If the bill is moving inside the collision radius of the
specified collision detection point then the function will always return true.
However, if the bill is moving less than is_moving_thresh then if it has been
replanned around once the function will return false and true otherwise

Arguments:
    b_handles::table
        table containg bill handles inside collision radius to check whether
        they need to be replanned around or not
    col_dis::float
        collision radius distance in m to check for from collision detection point
    col_dummy::int(object handle)
        object handle of specified collision detection point dummy

Returns:
      true
          if there is a bill inside the collision radius of the specified
          collision detection who's velocity is greater than is_moving_thresh
          or who's velocity is less but has not been planned around once yet
      false
          if no bill inside collision radius or if bill is inside collision
          radius but has already been replanned around once
]]--
function shouldReplanForBill(b_handles,col_dis,col_dummy)
    local res,index=isBillInCollisionRadius(b_handles,col_dis,col_dummy)
    --if there is a collision then check if it needs to be flagged or ignored
    --needs to be ignored if the bill is stationary and has been planned around once
    if res==1 then
     local bill_velocity=findLinVel(b_handles[index]) --finding absolute lin velocity of indexed bill
     --if a different bill has been detected in a collision then update last collision handle
     if b_handles[index]~=last_collision_bill_h then
        last_collision_bill_h=b_handles[index]
        last_time_col=sim.getSimulationTime() --reset last collision time
        num_paths_generated_since_collision=0 --reset planning counter
     end

     --if the bill in in collision is detected as being stationary
     if bill_velocity<is_moving_thresh then
        --if bill detected in collision is the same as the last collision and
        --we haven't planned around it yet then return 1 to flag as collision
        if b_handles[index]==last_collision_bill_h and num_paths_generated_since_collision<1 then
            res=1
        --if we have already planned around the bill then do NOT flag as collision
        else
            res=0
        end
        num_paths_generated_since_collision=num_paths_generated_since_collision+1
     end
    end

    --calculate time elapsed since last detected colision
    local time_since_col=findDt(last_time_col)
    if time_since_col>col_forget_time then
        last_collision_bill_h=-1
        last_time_col=sim.getSimulationTime()
    end

    return res
end

--[[
Creates a BLACK line visualising the primary path generated by OMPL inside the simulator

Arguments:
        path1::table(OMPLPath)
            table of path states created by OMPL

Returns:
      none
]]--
function visualizePrimaryPath(path1)
    --if there is no line container then create one
    if not line_container_1 then
        line_container_1=sim.addDrawingObject(sim.drawing_lines,4,0,-1,99999,{0.2,0.2,0.2})
    end
    sim.addDrawingObjectItem(line_container_1,nil)
    if path1 then
        local pc=#path1/3
        for z=1,pc-1,1 do
            local lineDat={path1[(z-1)*3+1],path1[(z-1)*3+2],init_robot_cylinder_pos[3],path1[z*3+1],path1[z*3+2],init_robot_cylinder_pos[3]}
            sim.addDrawingObjectItem(line_container_1,lineDat)
        end
    end
end

--[[
Creates a GREEN line visualising the secondary path generated by OMPL inside the simulator

Arguments:
        path1::table(OMPLPath)
            table of path states created by OMPL

Returns:
      none
]]--
function visualizeSecondaryPath(path2)
    --if there is no line container then create one
    if not line_container_2 then
        line_container_2=sim.addDrawingObject(sim.drawing_lines,7,0,-1,99999,{0.0,1,0,0.0})
    end
    sim.addDrawingObjectItem(line_container_2,nil)
    if path2 then
        local pc=#path2/3
        for z=1,pc-1,1 do
            local lineDat={path2[(z-1)*3+1],path2[(z-1)*3+2],init_robot_cylinder_pos[3],path2[z*3+1],path2[z*3+2],init_robot_cylinder_pos[3]}
            sim.addDrawingObjectItem(line_container_2,lineDat)
        end
    end
end

--[[
Finds the euclidian distance in m between two 2D points

Arguments:
        point1::table(2dPoint)
            first point to compare distance between
        point2::table(2dPoint)
            second point to compare distance between

Returns:
      distance_between_points::int
          euclidian distance between point 1 and 2 in m
]]--
function findDistanceBetweenTwoPoints(point_1,point_2)
    local dx=point_1[1]-point_2[1]
    local dy=point_1[2]-point_2[2]
    local distance_between_points=math.sqrt((dx)^2+(dy)^2)
    return distance_between_points
end

--[[
Finds the absolute linear velocity in m/s of a specified object in the workspace
derived using the simulatr function

Arguments:
    object::int(object handle)
        object handle of object to find linear velocity of

Returns:
    object_lin_vel::int
        linear velocity of specified object in m/s
]]--
function findLinVel(object)
    local object_vel=sim.getObjectVelocity(object)
    object_lin_vel = math.sqrt((object_vel[1]^2)+ (object_vel[2]^2))
    return object_lin_vel
end

--[[
Finds the difference in time between the current simulation time and the
simulation time provided as an argument

Arguments:
    last_t::int
        simulation time to compare with current simulation time

Returns:
    delta_t::int
        difference in time between current sim time and last_t
]]--
function findDt(last_t)
    current_t = sim.getSimulationTime()
    delta_t = current_t-last_t
    return delta_t
end

--[[
Similar to findDT however finds the difference in time between the current
system time and the system time provided as an arguement

Arguments:
    last_t::int
        system time to compare with current system time

Returns:
    delta_t::int
        difference in time between current system and last_t
]]--
function findRealTimeDt(last_t)
    current_t = sim.getSystemTime()
    delta_t = current_t-last_t
    return delta_t
end

--[[
Returns the wheel velocities for a 2 wheeled differential drive robot for a specified point
in the 2dDubins statespace using the Pure Pursuit path tracking algorithm. The steering
error between the specified point and the robot is calculated and linear velocity is made inversely
proportional to the steering error.

Arguments:
     goal_point::table(OMPL2Ddubins)
          2dDubins state to set as the lookahead point
      robot_handle::int(object handle)
          object handle to compute wheel velocities for
      robot_wheel_rad::float
          wheel radius of object in m
      robot_wheel_axel_len::float
          wheel axel length of object in m

Returns:
    phi_r::float
        wheel velocity for right wheel
    phi_l::float
        wheel velocity for left wheel
    dis::float
        distance in m between the robot and the goal point
]]--
function findDesWheelVel(goal_point,robot_handle,robot_wheel_rad,robot_wheel_axel_len)
    --converting goal point into robot's reference frame
    --get trasformation matrix of object
    local m=sim.getObjectMatrix(robot_handle,-1)
    --inverting object's transformation matrix
    sim.invertMatrix(m)
    --multiply inverted transformation matrix and object
    local goal_point_in_object_frame=sim.multiplyVector(m,goal_point)

    --calculating x^2 + y^2 = l^2
    l=math.sqrt((goal_point_in_object_frame[1]^2)+(goal_point_in_object_frame[2]^2))

    --calculating distance between goal point and object
    local dis=math.sqrt((goal_point_in_object_frame[1])^2+(goal_point_in_object_frame[2])^2)

    --calculating lambda (steering error in robot orientation to point)
    local lambda=(2*goal_point_in_object_frame[2])/(l^2)

    --calculating steering error for linear velocity
    robot_pos=sim.getObjectPosition(robot_handle,robot)
    dy=goal_point_in_object_frame[2]-robot_pos[2]
    dx=goal_point_in_object_frame[1]-robot_pos[1]

    --calculating steering error for linear velocity
    steering_angle=math.atan2(dy,dx)

    --setting linear velocity based on steering error
    V=(gain/(math.sqrt(steering_angle^2)))+minimum_movement_vel

    --enforcing a maximium velocity limit using the defined max linear velocity
    if(V>vel_des) then
        V=vel_des
    end

    --calculating angular velocity
    local omega=V*lambda

    --calculating velocities for left and right wheels
    local phi_r=(V+robot_wheel_axel_len*omega)/robot_wheel_rad
    local phi_l=(V-robot_wheel_axel_len*omega)/robot_wheel_rad

    --for the pioneer_p3dx robot wheel velocities are inverted so need to be multiplied by -1
    phi_r=phi_r*-1
    phi_l=phi_l*-1

    return phi_r,phi_l,dis
end

--[[
Finds the closest point on a specified path to an object returning the point and
the index of the closest point on the path

Arguments:
    object::int(object handle)
        object handle of object to find closest point to
    path_var::talbe(OMPLPath)
        OMPL path to find nearest point to object on
    look_ahead_jump::int
        number of states on path to jump through at a time when searching
        through path for nearest point

Returns:
    closest_point_on_path::table(2DPoint)
        2d Point that is the closest to the robot
    closest_index::int
        index on provided path that closest point is to object
    closest_distance::float
        Euclidian distance in m between the object and the closest point on path
]]--
function findClosestPointOnPathToObject(object,path_var,look_ahead_jump)
    local index=1 --index for point on path
    local previous_path_pos={path_var[index],path_var[index+1],init_rob_pos[3]} --setting to first point on path
    local index=4 --incrementing index to next point
    local cur_distance_between_points=0 --objects current distance away from point

    --lowest distance found on path
    local closest_distance=1000 --closest distance of point found on path (initialised to 1000)
    local closest_index=1 --index of point on path that is the closest

    --whilst we have not reached the end of the path search for closest point to object on path
    while index<(#path_var-look_ahead_jump) do
        --get object position
        local object_pos=sim.getObjectPosition(object,-1)
        --get position on path on current index
        local cur_path_pos={path_var[index],path_var[index+1],init_rob_pos[3]}
        --calculate distance between current point on path and object
        cur_distance_between_points=findDistanceBetweenTwoPoints(object_pos,cur_path_pos)

        --if the current point is the closest update minimum distance
        if cur_distance_between_points<closest_distance then
            closest_distance=cur_distance_between_points
            closest_index=index
        end
        --increment index
        index=index+look_ahead_jump
    end
    --set closest point on path after entire path has been itterated through
    closest_point_on_path={path_var[closest_index],path_var[closest_index+1],init_rob_pos[3]}
    --set closestPointDummy for visual and debugging purposes
    sim.setObjectPosition(closest_point_on_path_dummy,-1,closest_point_on_path)
    return closest_point_on_path,closest_index,closest_distance

end

--[[
Finds the index of the point on the specified path that is a specified distance distance away from the robot

NOTE: Used for placing the collision detection point infront of the robot

Arguments:
    path_fpop::table(OMPLpath)
        OMPL path to find point from
    cur_DisFromPoint::float
        robot's current distance from the point
    cur_index::int
        current index on the path that the robot is on
    point_step_dis::float
        distance to increment search for nearest point by in m for each itteration
    dis_from_point::float
        distance the point should be away from the robot in m
    look_ahead_jump::int
        number of states to skip per search itteration

Returns:
    cur_index::int
        index of point on path that is dis_from_point away from the robot
]]--
function findIndexOfPointOnPathFromDis(path_fpop,cur_DisFromPoint,cur_index,point_step_dis,dis_from_point,look_ahead_jump)
    --set initial point and initilise distance between points to 0
    local original_point={path_fpop[cur_index],path_fpop[cur_index+1],init_rob_pos[3]}
    local dis_bewtween_points=0

    --while the distance between current point and original point is less than the lookahed distance
    --keep itterating until current point on path is at least 1 lookahead distance away
    while dis_bewtween_points<dis_from_point and cur_index<(#path_fpop-look_ahead_jump) do
        local next_point={path_fpop[cur_index+look_ahead_jump],path_fpop[cur_index+1+look_ahead_jump ],init_rob_pos[3]}
        --calulate distance between points
        dis_between_points=findDistanceBetweenTwoPoints(original_point,next_point)
        --set index to next point
        cur_index=cur_index+look_ahead_jump

        --break out of loop if point distance is greater than lookahead point
        if dis_between_points>dis_from_point and cur_index<(#path_fpop-look_ahead_jump) then
            break
        end
    end
    return cur_index
end

--[[
Finds the lookahead point and index for the robot to follow for the
pure pursuit algorithm

Arguments:
    path_var::table(OMPL path)
        path to search for lookahead point
    starting_index::int
        index to start the search for the lookahead point from, should be set to
        the robot's current index on the specified path
    look_ahead_dis::float
        lookahead distance point should be from the robot
    look_ahead_jump::int
        number of states to jump over per search itteration on path
    robot_handle::int(object handle)
        robot's object handle to find lookahead point for

Returns:
    index::int
        index of lookahead point found on path
    cur_point_on_path::table(2dPoint)
        lookahead point in 2d point form
    obj_dis_from_point::float
        Euclidian distance of robot from lookahaed distance (used to calculate)
        steering angle as point may not be exactly lookahead distance away
]]--
function findLookAheadPoint(path_var,obj_dis_from_point,starting_index,look_ahead_dis,look_ahead_jump,object_handle)
    local index=starting_index
    local cur_point_on_path
    local object_pos

    --itterate through all points in path from starting_index onwards
    while index<(#path_var-look_ahead_jump) do
        --get objects current position
        object_pos=sim.getObjectPosition(object_handle,-1)
        --get position of current indexed point on path
        cur_point_on_path={path_var[index],path_var[index+1],init_rob_pos[3]}

        --calculate distance from object position to point on path
        obj_dis_from_point=findDistanceBetweenTwoPoints(object_pos,cur_point_on_path)

        --if the distance betweeen the object and the point is greater than the look ahead distance then break
        if obj_dis_from_point>look_ahead_dis then
            break
        --if the current point on paths distance away from the robot is still less than lookahead distance
        else
            --increment index to evaluate next point on path
            index=index+look_ahead_jump
        end
    end

    return index,cur_point_on_path,obj_dis_from_point
end

--[[
Locates the next collision free point on a specified path

Arguments:
    path_var::table(OMPLPath)
        OMPL path to find collision free point on
    cur_DisFromPoint::float
        distance from the robot to the collision detection point
    col_index::int
        index on the path where the collision detection point detected the bill
        inside the radius

Returns:
    collision_point_found::bool
        true
            if a collision was found on the path
        false
            if no colliion was detected on the path
    next_free_col_free_point_on_path::table(2dPoint)
        2d point of next collision free point on path

        returns end of path point if no collision is found on path
    col_index::int
        index of next collisio free point on specified path
]]--
function findNextCollisionFreePointOnPath(path_var,cur_DisFromPoint,col_index,point_step_dis,look_ahead_jump,col_dummy_h)
   local collision_point_found=false --has a point been found on the path that has a colision
   local next_free_col_free_point_on_path=nil --indicates the next collision free point on path after a collision has been detected

    --removing previously generated check spheres
    if collision_detection_points_placed>=1 then
        for c=1,collision_detection_points_placed,1 do
            sim.removeObject(check_sphere_dummys_h[c])
        end
    end

    --reseting sphere dummy index after removal of spheres
    collision_detection_points_placed=0
    check_sphere_dummys_h={}

   while col_index<(#path_var-look_ahead_jump) do
        --setting sphere dummy to computed point on path colision distance away
        target_point_on_path={path_var[col_index],path_var[col_index+1],init_rob_pos[3]}

        --setting collision dummy to computed target point
        sim.setObjectPosition(col_dummy_h,-1,target_point_on_path)

        --checking if bill is in colision radius of colision dummy
        bill_is_in_collision_r=isBillInCollisionRadius(bill_handles,collision_radius,col_dummy_h)

        --if bill is NOT in collision radius with computed point
        if bill_is_in_collision_r==0 then
            --break once we have found the next point that is not in collision after detecting collision
            if collision_point_found==true then
                next_free_col_free_point_on_path=target_point_on_path
                break
            end
            --creating smaller check sphere and adding it to table (to visualise NO collision on point)
            collision_detection_points_placed=collision_detection_points_placed+1
            check_sphere=sim.createPureShape(1,16,{0.03,0.1,0.1},0,nil)
            check_sphere_dummys_h[collision_detection_points_placed]=sim.getObjectHandle("Sphere"..(collision_detection_points_placed-1))
            sim.setObjectPosition(check_sphere,-1,target_point_on_path)
        end

        --if bill is in collision radius with computed point
        if bill_is_in_collision_r==1 then
            if collision_point_found==false then
                collision_point_found=true
            end
            --creating larger check sphere and adding it to table (to visualise collision on point)
            collision_detection_points_placed=collision_detection_points_placed+1
            check_sphere=sim.createPureShape(1,16,{0.05,0.1,0.1},0,nil)
            check_sphere_dummys_h[collision_detection_points_placed]=sim.getObjectHandle("Sphere"..(collision_detection_points_placed-1))
            sim.setObjectPosition(check_sphere,-1,target_point_on_path)
        end

        --if there has been no collision found on path after a set number of collision detection points have been placed
        --then decleare the path collision free
        if collision_point_found==false and collision_detection_points_placed>=max_num_of_col_detection_points then
          break
        end

        --finding next point on path that is the diameter of the colision radius away
        col_index=findIndexOfPointOnPathFromDis(path_var,cur_DisFromPoint,col_index,point_step_dis,(collision_radius*2),look_ahead_jump)
    end

    --if all the points on the path have been itterated through and no collision has been found then the
    --path is collision free (indicated by nil value) set to end goal
    if next_free_col_free_point_on_path==nil then
        print("path is collision free")
        next_free_col_free_point_on_path={path_var[#path_var-2],path_var[#path_var-1],init_rob_pos[3]}
    end

    return collision_point_found,next_free_col_free_point_on_path,col_index
end

--[[
Updates a provided dummy object to another object with the option to vertically
offset the position of the placed dummy if vert_offset is specified

Arguments:
    dummy_h::int(object handle)
        dummy object handle to place on the object
    target_h::int(object handle)
        object handle to place dummy on to
    vert_offset::float
        distance to vertically seperate placed dummy in m above object

Returns:
    none
]]--
function updateDummyPosToObject(dummy_h,target_h, vert_offset)
    --getting target object position and oreintation
    target_pos=sim.getObjectPosition(target_h,-1)
    target_oreint=sim.getObjectOrientation(target_h,-1)

    --if there is a vertical offset then apply offset to dummy position
    if vert_offset~=nil then
        target_pos={target_pos[1],target_pos[2],target_pos[3]+vert_offset}
    end

    --setting dummy to target's position and oreintation
    sim.setObjectPosition(dummy_h,-1,target_pos)
    sim.setObjectOrientation(dummy_h,-1,target_oreint)
end

--[[
Computes and returns an OMPL path using RRT-Connect and the 2dDubins statespace
for a given start and goal configuration.

Arguments:
    start_handle::int(object handle)
        object handle of start configuration
    goal_handle::int(object handle)
        object handle of goal configuration
    rob_handle::int(object handle)
        object handle of robot to compute path for
    time_to_compute::int
        maximum time path is allowed to attempt planning for before planning is
        considered to have failed

Returns:
    solution_path_found::bool
        true
            if solution path to the goal has been found
        false
            if no solution path could be found within time_to_compute
    path::table(OMPLPath2DDubins)
        computed path if solution path was found by OMPL in 2dDubins format

        nil if no solution path could be found
]]--
function computeOMPLPath(start_handle,goal_handle,rob_handle,time_to_compute)
      --checking to see if goal state is valid
      local goal_pos=sim.getObjectPosition(goal_handle,-1)
      sim.setObjectPosition(path_collision_detection_point_dummy_handle,-1,goal_pos)
      local is_collision_at_goal_pos=isBillInCollisionRadius(bill_handles,collision_radius,path_collision_detection_point_dummy_handle)

      --if goal position is collision free then compute path
      if is_collision_at_goal_pos==0 then
          --generate state space
          t=simOMPL.createTask('t')
          ss={simOMPL.createStateSpace('2d',simOMPL.StateSpaceType.dubins,start_config_cylinder_handle,{-10,-10},{10,10},1)}
          simOMPL.setStateSpace(t,ss)

          --setting dubins state space parameters
          simOMPL.setDubinsParams(ss[1],0.12,false)

          --choose algorithm for OMPL planning task
          simOMPL.setAlgorithm(t,simOMPL.Algorithm.RRTConnect)

          --setting collision pairs for OMPL to check for
          simOMPL.setCollisionPairs(t,{sim.getObjectHandle('Start_Config'),sim.getCollectionHandle('robot_cylinder_collection')})

          --define and set start state
          startpos=sim.getObjectPosition(start_config_cylinder_handle,-1)
          startorient=sim.getObjectOrientation(start_config_cylinder_handle,-1)
          startpose={startpos[1],startpos[2],startorient[3]}
          simOMPL.setStartState(t,startpose)

          --define and set goal state
          goalpos=sim.getObjectPosition(goal_handle,-1)
          goalorient=sim.getObjectOrientation(goal_handle,-1)
          goalpose={goalpos[1],goalpos[2],goalorient[3]}
          simOMPL.setGoalState(t,goalpose)

          --compute path using OMPL
          --wrapped in an error handler to protect against invalid states
          result_generated,solution_path_found,path=pcall(simOMPL.compute,t,time_to_compute,-1,800)

          --if error is found during planning then handle it
          if not result_generated then
              --if error found while planning then print error message to user
              print("Error found:"..solution_path_found)
              return false

          --if the path is empty then return false
          elseif path[1]==nil then
              print("Error found: path is empty")
              return false
          --otherwise computed path is valid so return it
          else
              return solution_path_found,path
          end

    --if goal position os obstructed then return false
    else
        print("Warning: Object is colliding with goal position")
        return false
    end
end

--[[
OMPL sometimes creates solution paths that it deems to be valid however do not
actually reach the goal. This function checks the distance of the final state
in the path and the goal config against a distance threshold to see if the solution
path generated ends at the specified goal and returns true if it does

Arguments:
    path_var::table(OMPLPath)
        OMPL path to check distance to goal for
    goal_config::table(object handle)
        object handle of goal configuration to check distance with
    distance_threshold::float
        minimum distance in m that goal must be to final point on path for it to
        be considered valid

Returns:
    true
        if distance between last point on path_var and goal config is less than
        distance threshold
    false
        if distance between last point on path_var and goal config is greater than
        distance threshold
]]--
function doesPathEndAtGoalConfig(path_var,goal_config,distance_threshold)
    --setting point to the last point on path
    last_point_on_path={path_var[#path_var-2],path_var[#path_var-1],init_rob_pos[3]}
    goal_point=sim.getObjectPosition(goal_config,-1)

    --if last point on path is closer than distance threshold to goal then path is valid
    if findDistanceBetweenTwoPoints(last_point_on_path,goal_point)<distance_threshold then
        print("Path is valid")
        return true
    else
        print("Path is INVALID")
        return false
    end
end

--[[
Generates an OMPL path in 2d Dubins state space using computeOMPLPath and checks
if computed path is valid whilst also recording the real time the path took to plan

Arguments:
    start_handle::int(object handle)
        object handle of start configuration
    goal_handle::int(object handle)
        object handle of goal configuration
    rob_handle::int(object handle)
        object handle of robot to compute path for
    time_to_compute::int
        maximum time path is allowed to attempt planning for before planning is
        considered to have failed
    goal_dis_thresh::float
        minimum distance in m that goal must be to final point on path for it to
        be considered valid
    states_to_jump::int
        number of states to jump per itteration whilst searching path

Returns:
    is_path_generated::bool
        true
            if path has been generated by OMPL AND last point is within goal_thresh
             distance of the specified goal configuration
        false
            if no path has been generated by OMPL OR path has been generated but
            last point on path not within goal_thresh distance
      path::table(OMPLPath)
          returns computed OMPL path
      index::int
          index to start navigating path at
      generate_new_path::bool
          true
              if new path should be generated if no path has been found or is invalid
          false
              if valid solution path has been found
      time_taken_to_plan::int
          realtime taken to plan in s
]]--
function generatePathForRobot(start_handle,goal_handle,robot_handle,time_to_compute,goal_dis_thresh,states_to_jump)
    --get time before path is computed to measure time taken to plan for evaluation
    local start_time=sim.getSystemTime()

    --compute OMPL path
    local is_path_generated,path=computeOMPLPath(start_handle,goal_handle,robot_handle,time_to_compute)

    --compute time taken to plan to delay the robot
    local time_taken_to_plan=findRealTimeDt(start_time)

    --don't log the time taken to plan the first path
    if total_num_paths_computed>=1 then
        total_planning_time=total_planning_time+time_taken_to_plan
    end

    --only stop generating paths when a solution path has been found
    if is_path_generated==true and doesPathEndAtGoalConfig(path,goal_handle,goal_dis_thresh)==true then
        generate_new_path=false --ensure path is only generated once
        index=1+states_to_jump --setting initial path index to init jump to skip specified states on path
        return is_path_generated,path,index,generate_new_path,time_taken_to_plan
    --print to user that solution path has NOT been found and needs to be recalculated
    else
        print("========================================================================")
        print("WARNING: Solution path NOT found (perhaps goal is obstructed?): RETRYING")
        print("========================================================================")
        return false,nil,nil,true,time_taken_to_plan
    end
end

--[[
Allows the robot to follow the primary path by placing collision detection point,
checking for humans in collision radius. If humans are detected in the collision
radius the robot will stop and set follow_secondary_path=true so the secondary
path can be followed.

The function places the lookahead point, calculates wheel
velocities so the robot can follow the steering arc and checking to see if the robot
has reached the end goal configuration at which point it will stop.

Arguments:
    path_var::table(OMPLPath)
        specifies the primary path to follow

Returns:
    None
]]--
function robotFollowPrimaryPath(path_var)
    --set lookahead point on path
    closest_pos_on_path,closest_index,closest_distance_on_path=findClosestPointOnPathToObject(robot,path_var,3)
    lookahed_index,lookahead_point,look_ahead_dis=findLookAheadPoint(path_var,closest_distance_on_path,closest_index,rob_look_ahead_dis,3,robot)
    sim.setObjectPosition(test_dummy,-1,lookahead_point)

    local target_point=lookahead_point
    i=lookahed_index

     --set colision dummies position on path which will check for collisions on path with bill's
    col_i=findIndexOfPointOnPathFromDis(path_var,dis_robToPoint,closest_index,rob_point_step_dis,safety_distance,3 ) --find point on path that is specified distance away
    collision_dummy_point={path_var[col_i],path_var[col_i+1],init_rob_pos[3]}
    sim.setObjectPosition(robot_collision_detection_point_dummy_handle,-1,collision_dummy_point) --set collision dummy to position

    --if no bill is detected as being in collision radius and a path has been generated by OMPL
    --calculate and set wheel velocities for robot to head towards target point
    local bill_in_collision_radius=shouldReplanForBill(bill_handles,collision_radius,robot_collision_detection_point_dummy_handle)
    if bill_in_collision_radius~=1 and generate_new_primary_path==false then
        ----find velocities for robot wheels
        om_right,om_left,dis_robToPoint=findDesWheelVel(target_point,robot,wheel_rad,wheel_sep,vel_des)

        --setting computed velocities for robot wheels
        sim.setJointTargetVelocity(rm,-om_right)
        sim.setJointTargetVelocity(lm,-om_left)

    --if bill IS detected as colliding in collision radius then stop the robot
    elseif bill_in_collision_radius==1 then
        --setting index collision was detected at on path
        index_collision_detected_at=col_i

        --find next collision free point on primary path acting as goal of secondary path
        local collision_on_path,secondary_goal_point,secondary_goal_index=findNextCollisionFreePointOnPath(path_var,dis_robToPoint,col_i,
        point_step_dis,rob_look_ahead_jump,path_collision_detection_point_dummy_handle)

        --if collision is detected then STOP and follow secondary path to naviagate around bill on primary path
        if collision_on_path==true then
            print("bill detected in PRIMARY path")
            --setting robot wheel velocities for STOP
            sim.setJointTargetVelocity(rm,0)
            sim.setJointTargetVelocity(lm,0)
            --setting it so secondary path can be followed instead of primary to navigate around
            --object on primary path

            print("follow secondary path = ")
            print(follow_secondary_path)
            --setting generate secondary path flag to true to an OMPL paht can be created
            generate_new_secondary_path=true

        else
            print("bill detected but ignored")
        end
    end

    --if goal point has been reached then stop the robot
    if i>=#path_var-rob_look_ahead_jump and dis_robToPoint<goal_dis_thresh then
        end_goal_reached=true
        print("goal reached!!!")
        --setting robot wheel velocities for STOP
        sim.setJointTargetVelocity(rm,0)
        sim.setJointTargetVelocity(lm,0)

        --log the trial results in a logfile
        logTrialResultsInFile(file_name,true)

        --stop simulation once goal has been reached
        sim.stopSimulation()
    end
end

--[[
Allows the robot to follow the primary path by placing collision detection point,
checking for humans in collision radius. If humans are detected in the collision
radius the robot will stop and set follow_secondary_path=true so the secondary
path can be followed.

The function places the lookahead point, calculates wheel
velocities so the robot can follow the steering arc and checking to see if the robot
has reached the end goal configuration at which point it will stop.

Arguments:
    path_var::table(OMPLPath)
        specifies the primary path to follow

Returns:
    None
]]--
function robotFollowSecondaryPath(path_var)
    --set lookahead point on path
    closest_pos_on_path,closest_index,closest_distance_on_path=findClosestPointOnPathToObject(robot,path_var,3)
    lookahed_index,lookahead_point,look_ahead_dis=findLookAheadPoint(path_var,closest_distance_on_path,closest_index,rob_look_ahead_dis,3,robot)
    sim.setObjectPosition(test_dummy,-1,lookahead_point)

    local target_point=lookahead_point
    s=lookahed_index

     --set colision dummies position on path which will check for collisions on path with bill's
    col_i=findIndexOfPointOnPathFromDis(path_var,dis_robToPoint,closest_index,rob_point_step_dis,safety_distance,3 ) --find point on path that is specified distance away
    collision_dummy_point={path_var[col_i],path_var[col_i+1],init_rob_pos[3]}
    sim.setObjectPosition(robot_collision_detection_point_dummy_handle,-1,collision_dummy_point) --set collision dummy to position

    --if no bill is detected as being in collision radius and a path has been generated by OMPL
    --calculate and set wheel velocities for robot to head towards target point
    local bill_in_collision_radius=shouldReplanForBill(bill_handles,collision_radius,robot_collision_detection_point_dummy_handle)
    if bill_in_collision_radius~=1 and generate_new_secondary_path==false then
        ----find velocities for robot wheels
        om_right,om_left,dis_robToPoint=findDesWheelVel(target_point,robot,wheel_rad,wheel_sep,vel_des)

        --setting computed velocities for robot wheels
        sim.setJointTargetVelocity(rm,-om_right)
        sim.setJointTargetVelocity(lm,-om_left)

    --if bill IS detected as colliding in collision radius then stop the robot
    elseif bill_in_collision_radius==1 then
        --setting robot wheel velocities for STOP
        sim.setJointTargetVelocity(rm,0)
        sim.setJointTargetVelocity(lm,0)
        --generate new path
        generate_new_secondary_path=true
    end

    --if goal point has been reached then stop the robot
    if  dis_robToPoint<goal_dis_thresh then
        --stop following primary path and follow secondary path instead
        follow_secondary_path=false
        --new secondary path needs to be generated if there is a new collision
        generate_new_secondary_path=true
        secondary_solution_path_generated=false
        print("SECONDARY goal reached!!!")
        --setting robot wheel velocities for STOP
        sim.setJointTargetVelocity(rm,0)
        sim.setJointTargetVelocity(lm,0)
    end
end

--main system call
function sysCall_actuation()
    --finding delta time since last execution cycle
    dt=findDt(last_time_rate)

    --robot should wait for the amount of time taken to plan the path regardless
    --of whether it was successful or not to simulate the time taken to plan
    if wait_for_delay==true then
       print("simulating time taken to plan!")
       --stop the robot
       sim.setJointTargetVelocity(rm,0)
       sim.setJointTargetVelocity(lm,0)
        --calculate time betweeen last computed path
        dt_since_failed_path=findDt(time_path_computed_at)
        print("time waited = "..dt_since_failed_path.." out of: "..time_to_wait_after_computing_path)

        --if the time elapsed is greater than the delay time resume execution
        if dt_since_failed_path>time_to_wait_after_computing_path then
            wait_for_delay=false
            time_to_wait_after_computing_path=0
        end
    end

    --only execute if time elapsed is gretder than specified rate and the goal point
    --has NOT been reached by the robot and there is no delay required
    if dt>rate and end_goal_reached==false and wait_for_delay==false  then
        --updating cylinder to robot's position ready for OMPL path planning
        updateDummyPosToObject(start_config_cylinder_handle, robot, 0.01)

        --finding robot's current linear velocity
        rob_lin_vel = findLinVel(robot)

        --if the robot has deviated so far from the path then we need to replan a new path
        --check if the primary path has been created before seeing how far the robot is away
        if primary_solution_path_generated==true then
            if closest_distance_on_path>max_robot_deviation_from_path then
                print("Robot is too far away from path")
                print("repplanning")
                --stop the robot first to allow a new path to be planned
                sim.setJointTargetVelocity(rm,0)
                sim.setJointTargetVelocity(lm,0)
                --generate a new secondary path to get the robot back on the path
                generate_new_secondary_path=true
                follow_secondary_path=true
            end
         end

        --generate OMPL primary path but only generate if flag is set and once robot has stopped to
        --prevent invalid initial starting state
        if generate_new_primary_path==true and rob_lin_vel<is_moving_thresh then
            --compute Primary OMPL path
            primary_solution_path_generated,OMPL_primary_path,i,generate_new_primary_path,time_spent_planning=generatePathForRobot(start_config_dummy_handle,
            end_goal_config_dummy_handle,robot,10,goal_dis_thresh,rob_look_ahead_jump)

            --visualize path in sim
            visualizePrimaryPath(OMPL_primary_path)

            wait_for_delay=true
            time_to_wait_after_computing_path=time_spent_planning
            time_path_computed_at=sim.getSimulationTime()

            if primary_solution_path_generated==true then
              --increment total number of paths calculated for testing
              total_num_paths_computed=total_num_paths_computed+1
            else
                print("PRIMARY path has FAILED to compute")
                wait_for_delay=true
                last_failed_path_time=sim.getSimulationTime()
            end
        end

        --generate OMPL secondary path but only generate if flag is set and once robot has stopped to
        --prevent invalid initial starting state
        if generate_new_secondary_path==true and rob_lin_vel<is_moving_thresh then
            --find closest point on the primary path to the robot to start search from
            local closest_point_on_primary_path,closest_index_on_primary_path=findClosestPointOnPathToObject(robot,OMPL_primary_path,rob_look_ahead_jump)

            --find next collision free point from the closest point to the robot on primary path for the goal of secondary path
            collision_on_path,secondary_goal_point,secondary_goal_index=findNextCollisionFreePointOnPath(OMPL_primary_path,dis_robToPoint,closest_index_on_primary_path,
            rob_point_step_dis,rob_look_ahead_jump,robot_collision_detection_point_dummy_handle)

            --if there is a collision (human) detected on the path then set the secondary goal to the commputed collision free point
            if collision_on_path then
                i=secondary_goal_index
                --set goal configuration position and oreination for secondary path to secondary goal point
                sim.setObjectPosition(second_goal_config_dummy_handle,-1,secondary_goal_point)
                sim.setObjectOrientation(second_goal_config_dummy_handle,-1,{0,0,(OMPL_primary_path[secondary_goal_index+2]+(math.pi*2))})

            else
                --if a secondary path HAS already been computed and no collisiion has been detected then set the secondary goal point to the nearest
                --point on the primary path offset by 1.5 * the collision radius so the robot is not rejoining at a perpendicular angle
                if follow_secondary_path==true then
                     offset_closest_index,offset_closest_point=findLookAheadPoint(OMPL_primary_path,0,closest_index_on_primary_path,(1.5*safety_distance),3,robot)
                     i=offset_closest_index
                     sim.setObjectPosition(second_goal_config_dummy_handle,-1,offset_closest_point)
                     sim.setObjectOrientation(second_goal_config_dummy_handle,-1,{0,0,(OMPL_primary_path[offset_closest_index+2]+(math.pi*2))})
                --if a secondary path has not been computed and there is no collision detected then resume following the primary path and do NOT
                --generate a secondary path
                else
                    follow_secondary_path=false
                    generate_new_secondary_path=false
                end
            end

            --ensure that path still needs to be generated
            if generate_new_secondary_path==true then
                --compute Secondary OMPL path
                secondary_solution_path_generated,OMPL_secondary_path,s,generate_new_secondary_path,time_spent_planning=generatePathForRobot(start_config_dummy_handle,
                second_goal_config_dummy_handle,robot,2.5,goal_dis_thresh,rob_look_ahead_jump)

                --set robot to wait for the time taken to plan the path
                --to simulate the time taken in real life
                wait_for_delay=true
                time_to_wait_after_computing_path=time_spent_planning
                time_path_computed_at=sim.getSimulationTime()
            end

            --
            if secondary_solution_path_generated==true then
                --visualize path in sim
                visualizeSecondaryPath(OMPL_secondary_path)
                --set flag to false so no more path are generated
                generate_new_secondary_path=false
                follow_secondary_path=true
                --increment total number of paths calculated for testing
                total_num_paths_computed=total_num_paths_computed+1
            --if a secondary solution path was NOT found then delay before computing a new one
            else
              print("SECONDARY path has FAILED to compute")
              wait_for_delay=true
              last_failed_path_time=sim.getSimulationTime()
            end
        end

        --only start following the path once a valid solution path is generated by OMPL
        if primary_solution_path_generated==true then
            --only follow primary path if we aren't following the seondary path
            if follow_secondary_path==false and generate_new_secondary_path==false then
                robotFollowPrimaryPath(OMPL_primary_path)
            end

            --follow secondary path if follow secondary path flag is set
            if follow_secondary_path==true then
                if secondary_solution_path_generated==true and generate_new_secondary_path==false then
                    robotFollowSecondaryPath(OMPL_secondary_path)
                end
            end
        end
        --set time taken to go through execution to current sim time
        last_time_rate=sim.getSimulationTime()
    end
end

--***************************FOR TESTING AND EVALUATION**************************

--[[
Detects if an object collection is colliding with an object or not using the simulator
collision detection methods

Arguments:
        collection_handle::int(collection handle)
            collection_handle to check for collisions

Returns:
    true
        if a collision IS detected
    false
        if NO collision is detected
]]--
function isThereACollisionWithObject(collection_handle)
    --check if there is a collision with the collection of the object
    local result,pairHandles=sim.checkCollision(collection_handle,sim.handle_all)

    if result>0 then
        return true
    else
        return false
    end
end


--[[
writes all recorded performance data from the trial into a file within the
CoppeliaSim main directory

Arguments:
    f_name::string
        name to save logfile as
    was_trial_successful
        did the robot reach the end goal in the time limit or not

Returns:
    none

]]--
function logTrialResultsInFile(f_name,was_trial_successful)
    --open up file and append trial data
    file = io.open(f_name, "a")

    --log time trial ended
    current_os_time=os.date('%d/%m/%Y[%H:%M:%S]')
    file:write('\n'..current_os_time..'\t')
    --log number of collisions with robot and humans in trial
    file:write(number_of_collisions_with_humans..'\t')
    --log total number of paths replanned
    file:write((total_num_paths_computed-1)..'\t')
    --log length of time spent planning
    file:write(string.format('%.3f\t',total_planning_time))
    --log total distance robot has traveled
    file:write(string.format('%.3f\t',total_distance_traveled))
    --log if trial was successful or not
    file:write(tostring(was_trial_successful)..'\t')

    --log time to get to goal if successful
    if was_trial_successful==true then
        goal_time=(sim.getSystemTime()-system_start_time)
        file:write(string.format('%.3f\t',goal_time))
    --if the trial was unsuccessful just write nil
    else
        file:write('nil\t')
    end

    --close and save the logfile in CoppeliaSim directory
    file:close()
end

--sensing system call for testing
function sysCall_sensing()
    if dt>sensing_rate then
        --checking if maximum time has elapsed for robot to reach the goal
        --if so then stop the simulation and end the trial
        if (sim.getSimulationTime()-simulation_start_time)>max_time_allowed_to_reach_goal and not sim_is_stopping then
            print("*************************************")
            print("Maximum time to reach goal has elapsed")
            print("Trial has FAILED!!!")
            print("*************************************")

            --log trial results in logfile
            logTrialResultsInFile(file_name,false)
            --end the simulation
            sim_is_stopping = true
            sim.stopSimulation()

        else
            --===========DETECTING NUMBER OF COLLISIONS WITH HUMANS==========
            local robot_is_colliding=isThereACollisionWithObject(robot_collection_handle)

            --getting time since last collision was detected
            local time_since_last_collision=findDt(last_collision_time)

            if robot_is_colliding and time_since_last_collision>time_to_wait_between_collisions then
                print("**************")
                print("ROBOT IS COLLIDING")
                print("TIME SINCE LAST COLLISION")
                print(time_since_last_collision)
                last_collision_time=sim.getSimulationTime()
                number_of_collisions_with_humans=number_of_collisions_with_humans+1
                print("Number of collisions with humans is: ".. number_of_collisions_with_humans)
            end

            --==========CALCULATING TOTAL DISTANCE COVERED BY ROBOT==========
            robot_current_position=sim.getObjectPosition(robot,-1)
            --calculate the distance traveled since the last simulation time step
            --distance_traveled_since_step=findDistanceBetweenTwoPoints(robot_current_position,robot_last_position)
            distance_traveled_since_step=findDistanceBetweenTwoPoints(robot_current_position,robot_last_position)
            --add this distance to the total distance traveled
            total_distance_traveled=total_distance_traveled+distance_traveled_since_step
            --reset robot's last position to the current position
            robot_last_position=robot_current_position
        end
    end
end
